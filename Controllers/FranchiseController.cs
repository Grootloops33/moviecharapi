﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using moviecharapi.Data;
using moviecharapi.Models.Domain;
using moviecharapi.Models.DTO.Character;
using moviecharapi.Models.DTO.Franchise;
using moviecharapi.Models.DTO.Movie;
using System.Net.Mime;

namespace moviecharapi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class FranchiseController : ControllerBase
    {
        private readonly MovieDbContext _context;
        private readonly IMapper _mapper;

        public FranchiseController(MovieDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        /// <summary>
        /// Gets a list of all franchises in the database
        /// </summary>
        /// <returns>A list of all of the franchises in the database</returns>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<FranchiseReadDTO>>> GetAllFranchises()
        {
            if (_context.Franchises == null)
            {
                return NotFound("Database contains no franchises");
            }

            var franchises = await _context.Franchises.Include(c => c.Movies).ToListAsync();
            var readFranchisesDTO = _mapper.Map<List<FranchiseReadDTO>>(franchises);

            return Ok(readFranchisesDTO);
        }

        /// <summary>
        /// Gets a franchise based on the given id
        /// </summary>
        /// <param name="id">The id of the franchise</param>
        /// <returns>The franchise with the given id if found</returns>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet("{id}")]
        public async Task<ActionResult<FranchiseReadDTO>> GetFranchiseById(int id)
        {
            if (_context.Franchises == null)
            {
                return NotFound("Database contains no franchises");
            }

            var franchise = await _context.Franchises.Include(f => f.Movies).Where(f => f.Id == id).FirstOrDefaultAsync();

            if (franchise == null)
            {
                return NotFound($"Database contains no franchise with id {id}");
            }

            var franchiseDTO = _mapper.Map<FranchiseReadDTO>(franchise);

            return Ok(franchiseDTO);
        }
        /// <summary>
        /// Gets all the movies in a franchise
        /// </summary>
        /// <param name="id">The id of the franchise that needs to show all the movies</param>
        /// <returns>A list of movies that are in the franchise</returns>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet("{id}/movies")]
        public async Task<ActionResult<IEnumerable<MovieReadDTO>>> GetAllMoviesInFranchiseById(int id)
        {
            var movies = await _context.Movies
                .Where(movie => movie.FranchiseId == id).ToListAsync();

            if (movies == null)
            {
                return NotFound($"Database contains no movies with franchise id {id}");
            }

            var moviesReadDtos = new List<MovieReadDTO>();
            foreach (var movie in movies)
            {
                moviesReadDtos.Add(_mapper.Map<MovieReadDTO>(movie));
            }

            return Ok(moviesReadDtos);
        }


        /// <summary>
        /// Gets all characters in the given franchise
        /// </summary>
        /// <param name="id">The id of the franchise</param>
        /// <returns>A list of character DTOs in the given franchise</returns>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet("{id}/characters")]
        public async Task<ActionResult<IEnumerable<CharacterReadDTO>>> GetAllCharactersInFranchiseById(int id)
        {
            var movies = await _context.Movies
                .Where(movie => movie.FranchiseId == id).ToListAsync();

            if (movies == null)
            {
                return NotFound($"Database contains no characters in movies with franchise id {id}");
            }

            var charactersInFranchise = await GetCharactersListFromMovieIds(movies);
            var uniqueCharacters = new List<CharacterReadDTO>();

            foreach (var character in charactersInFranchise)
            {
                if (uniqueCharacters.Find(c => c.Id == character.Id) == null)
                {
                    uniqueCharacters.Add(_mapper.Map<CharacterReadDTO>(character));
                }
            }
            return Ok(uniqueCharacters);
        }

        /// <summary>
        /// Updates a franchise with given ID to the given values
        /// </summary>
        /// <param name="id">The id of the franchise that needs to be updated</param>
        /// <param name="franchise">All the values required for the updated franchise</param>
        /// <returns>An action result that represents how the franchise update went</returns>
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateFranchise(int id, [FromBody] FranchiseUpdateDTO franchise)
        {
            if (id != franchise.Id)
            {
                return BadRequest($"Request body id doesn't match request body franchise-object id");
            }

            try
            {
                var domainFranchise = _mapper.Map<Franchise>(franchise);

                _context.Entry(domainFranchise).State = EntityState.Modified;

                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FranchiseExists(id))
                {
                    return NotFound($"No franchise in database with id {id}");
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        /// <summary>
        /// Updates the movies in a given franchise to the given movies
        /// </summary>
        /// <param name="franchiseId">The id of the franchise that needs to be updated</param>
        /// <param name="movieIds">The ids of the movies that need to be added to the franchise</param>
        /// <returns>An action result that represents how the franchise movie update went</returns>
        /// 
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [HttpPut("{id}/movies")]
        public async Task<ActionResult> UpdateMoviesInFranchise(int franchiseId, [FromBody] List<int> movieIds)
        {
            try
            {
                List<Movie> selectedMovies = await GetMoviesListFromIds(movieIds);

                for (int i = 0; i < selectedMovies.Count; i++)
                {
                    if (selectedMovies[i] == null) { return NotFound(); }
                    selectedMovies[i].FranchiseId = franchiseId;
                    _context.Entry(selectedMovies[i]).State = EntityState.Modified;
                }

                await _context.SaveChangesAsync();
            }
            catch
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }

            return NoContent();
        }

        /// <summary>
        /// Adds a new franchise to the database
        /// </summary>
        /// <param name="franchise">The values for the new franchise that needs to be added</param>
        /// <returns>An action result that represents how the franchise adding went</returns>
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [HttpPost]
        public async Task<ActionResult> AddFranchise([FromBody] FranchiseCreateDTO franchise)
        {
            var domainFranchise = _mapper.Map<Franchise>(franchise);

            try
            {
                _context.Franchises.Add(domainFranchise);

                await _context.SaveChangesAsync();
            }
            catch
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }

            var readFranchiseDTO = _mapper.Map<FranchiseReadDTO>(domainFranchise);
            return CreatedAtAction("GetFranchiseById", new { id = readFranchiseDTO.Id }, readFranchiseDTO);
        }

        /// <summary>
        /// Removes a franchise from the database with given id
        /// </summary>
        /// <param name="deleteFranchiseDTO">The id for the franchise that needs to be deleted</param>
        /// <returns>An action result that represents how the franchise deletion went</returns>
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteFranchise(FranchiseDeleteDTO deleteFranchiseDTO)
        {
            var franchise = await _context.Franchises.FindAsync(deleteFranchiseDTO.Id);
            if (franchise == null)
            {
                return NotFound("Database contains no franchises");
            }

            _context.Franchises.Remove(franchise);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        /// <summary>
        /// Check whether a franchise exists
        /// </summary>
        /// <param name="id">The id that need to be checked</param>
        /// <returns>True if the franchise exists else it returns false</returns>
        private bool FranchiseExists(int id)
        {
            return _context.Franchises.Any(e => e.Id == id);
        }

        /// <summary>
        /// Get all the movies with the given ids
        /// </summary>
        /// <param name="movieIds">A list of movie ids</param>
        /// <returns>A list of movies based on the given ids</returns>
        private async Task<List<Movie>> GetMoviesListFromIds(List<int> movieIds)
        {
            List<Movie> selectedMovies = new List<Movie>();

            for (int i = 0; i < movieIds.Count; i++)
            {
                Movie? movie = await _context.Movies.Include(c => c.Characters).Where(c => c.Id == movieIds[i]).FirstOrDefaultAsync();
                selectedMovies.Add(movie);
            }

            return selectedMovies;
        }

        /// <summary>
        /// Get all characters that are listed in the list of movies
        /// </summary>
        /// <param name="movies">A list of movies</param>
        /// <returns>A list of characters that are used in the movies</returns>
        private async Task<List<Character>> GetCharactersListFromMovieIds(List<Movie> movies)
        {
            List<int> selectedCharacters = new List<int>();
            List<Character> characterReadDTOs = new List<Character>();
            for (int i = 0; i < movies.Count; i++)
            {
                List<int> characters = await _context.CharacterMovie
                    .Where((relation) => relation.MovieId == movies[i].Id)
                    .Select((relation) => relation.CharacterId).ToListAsync();

                selectedCharacters.AddRange(characters);
            }
            foreach (var id in selectedCharacters)
            {
                characterReadDTOs.AddRange(await _context.Characters.Where(c => c.Id == id).ToListAsync());
            }

            return characterReadDTOs;
        }
    }
}

