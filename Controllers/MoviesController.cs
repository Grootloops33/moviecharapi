﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using moviecharapi.Data;
using moviecharapi.Models.Domain;
using moviecharapi.Models.DTO.Character;
using moviecharapi.Models.DTO.Movie;
using System.Net.Mime;

namespace moviecharapi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]

    public class MoviesController : ControllerBase
    {
        private readonly MovieDbContext _context;
        private readonly IMapper _mapper;

        public MoviesController(MovieDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Movies
        /// <summary>
        /// Gets a list of all movies in a database
        /// </summary>
        /// <returns>A list of all movies in the database</returns>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<MovieReadDTO>>> GetMovies()
        {
            var movies = _context.Movies.ToListAsync();
            if (_context.Movies == null)
            {
                return NotFound("Database contains no movies");
            }
            return _mapper.Map<List<MovieReadDTO>>(await movies);
        }

        /// <summary>
        /// Gets a movie with a specified id
        /// </summary>
        /// <param name="id">Movie id to retrieve</param>
        /// <returns>The movie with given id if found</returns>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet("{id}")]
        public async Task<ActionResult<MovieReadDTO>> GetMovie(int id)
        {
            if (_context.Movies == null)
            {
                return NotFound("Database contains no movies");
            }
            var movie = await _context.Movies.FindAsync(id);

            if (movie == null)
            {
                return NotFound($"No movie with id {id} found in database");
            }
            return Ok(_mapper.Map<MovieReadDTO>(movie));
        }

        /// <summary>
        /// Gets a list of all characters in a movie with a specified id
        /// </summary>
        /// <param name="id">The movie id to retrieve characters for</param>
        /// <returns>The movie with given id if found</returns>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet("{id}/characters")]
        public async Task<ActionResult<List<CharacterReadDTO>>> GetMovieCharacters(int id)
        {
            if (_context.CharacterMovie == null)
            {
                return NotFound("No character-movie currently in database");
            }

            var characters = await _context.CharacterMovie
                .Where((relation) => relation.MovieId == id)
                .Select((relation) => relation.Character)
                .Select(character => _mapper.Map<CharacterReadDTO>(character))
                .ToListAsync();
            if (characters.Count() == 0)
            {
                return NotFound($"Movie with id {id} has no characters in database");
            }
            return Ok(characters);
        }
        /// <summary>
        /// Updates movie with specific id
        /// </summary>
        /// <param name="id">A movie id to find and update in database</param>
        /// <param name="movie">Updated Movie object</param>
        /// <returns>An action result with a status code</returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateMovie(int id, [FromBody] MovieUpdateDTO movie)
        {
            var current = _context.Movies.Find(id);
            if (current == null)
            {
                _context.Movies.Add(_mapper.Map<Movie>(movie));
                return NoContent();
            }
            else
            {
                try
                {
                    _context.Entry(current).CurrentValues.SetValues(movie);
                    _context.Entry(current).State = EntityState.Modified;

                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!MovieExists(id))
                    {
                        return NotFound($"Movie with id {id} not found in database");
                    }
                    else
                    {
                        throw;
                    }
                }
            }
            await _context.SaveChangesAsync();

            return NoContent();
        }
        /// <summary>
        /// Updates characters of movie with specified id
        /// </summary>
        /// <param name="id">A movie id to find and update in database</param>
        /// <param name="characterIds">List of characters to add to movie</param>
        /// <returns>An action result with a status code</returns>
        [HttpPut("{id}/characters")]
        public async Task<IActionResult> UpdateMovieCharacters(int id, [FromBody] IEnumerable<int> characterIds)
        {
            var current = _context.Movies.Find(id);
            List<CharacterMovie> currentRelations = await _context.CharacterMovie.Where(characterMovie => characterMovie.MovieId == id).ToListAsync();
            if (current == null || characterIds.Count() == 0)
            {
                return BadRequest("Invalid object provided");
            }
            else
            {
                try
                {
                    List<CharacterMovie> updatedCharacterMovies = new List<CharacterMovie>();
                    foreach (var i in characterIds)
                    {
                        CharacterMovie newEntry = new CharacterMovie
                        {
                            CharacterId = i,
                            MovieId = id
                        };
                        // Add only new relations to avoid PK conflict
                        if (!currentRelations.Contains(newEntry))
                            updatedCharacterMovies.Add(newEntry);
                    }
                    if (updatedCharacterMovies.Count == 0)
                    {
                        return BadRequest("No changes could be made as all the provided values already existed in database");
                    }
                    _context.Entry(current).CurrentValues.SetValues(current.Characters = updatedCharacterMovies);
                    _context.Entry(current).State = EntityState.Modified;

                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!MovieExists(id))
                    {
                        return NotFound("Movie not found in database");
                    }
                    else
                    {
                        throw;
                    }
                }
            }
            await _context.SaveChangesAsync();

            return Ok($"Put request for movie with id {id} succesful");
        }
        /// <summary>
        /// Adds a new movie to the database
        /// </summary>
        /// <param name="movieCreateDTO">A movie object to add to the database</param>
        /// <returns>An action result with a status code</returns>
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [HttpPost]
        public async Task<ActionResult<MovieCreateDTO>> PostMovie([FromBody] MovieCreateDTO movieCreateDTO)
        {
            var movie = _mapper.Map<Movie>(movieCreateDTO);
            if (_context.Movies == null)
            {
                return Problem("Entity set 'MovieDbContext.Movies' is null.");
            }
            try
            {
                _context.Movies.Add(movie);
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
                //return StatusCode(StatusCodes.Status500InternalServerError);
            }
            var newMovie = _mapper.Map<MovieReadDTO>(movie);
            return CreatedAtAction("GetMovie", new { id = newMovie.Id }, newMovie);
        }

        /// <summary>
        /// Removes movie with given id from the database
        /// </summary>
        /// <param name="id">Id of movie to delete from database</param>
        /// <returns>An action result with a status code</returns>
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteMovie(int id)
        {
            var movie = await _context.Movies.FindAsync(id);
            if (_context.Movies == null)
            {
                return NotFound($"No movies in database");
            }
            if (movie == null)
            {
                return NotFound($"No movies in database under id {id}");
            }

            _context.Movies.Remove(movie);
            await _context.SaveChangesAsync();

            return NoContent();
        }
        /// <summary>
        /// Checks if the movie exists
        /// </summary>
        /// <param name="id">Id to find in database</param>
        /// <returns>True if movie exists in database, false if not</returns>
        private bool MovieExists(int id)
        {
            return (_context.Movies?.Any(e => e.Id == id)).GetValueOrDefault();
        }
    }
}
