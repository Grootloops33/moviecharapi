﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Net.Mime;
using AutoMapper;
using moviecharapi.Data;
using moviecharapi.Models.Domain;
using moviecharapi.Models.DTO.Character;

namespace moviecharapi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class CharactersController : ControllerBase
    {
        private readonly MovieDbContext _context;
        // Add automapper via DI
        private readonly IMapper _mapper;

        public CharactersController(MovieDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        /// <summary>
        /// Gets all the characters in the database.
        /// </summary>
        /// <returns>A list of all the characters in a database</returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CharacterReadDTO>>> GetCharacters()
        {
            if (_context.Characters == null)
            {
                return NotFound("Database contains no characters");
            }
            var readCharacterDTO = _mapper.Map<List<CharacterReadDTO>>(await _context.Characters.ToListAsync());
            return Ok(readCharacterDTO);
        }

        /// <summary>
        /// Gets a specific character by their Id
        /// </summary>
        /// <param name="id">The id of the character</param>
        /// <returns>A character with the given id if found</returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<CharacterReadDTO>> GetCharacter(int id)
        {
            var character = await _context.Characters.FindAsync(id);

            if (character == null)
            {
                return NotFound($"Database contains no character with id {id}");
            }

            return Ok(_mapper.Map<CharacterReadDTO>(character));
        }

        /// <summary>
        /// Updates a character with given id in database.
        /// </summary>
        /// <param name="id">The id of the character that is updated</param>
        /// <param name="character">A character object with all the desired changes</param>
        /// <returns>An action result with a status code of the request</returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCharacter(int id, [FromBody] CharacterUpdateDTO character)
        {
            if (id != character.Id)
            {
                return BadRequest("Request body id doesn't match request body character-object id");
            }
            Character domainCharacter = _mapper.Map<Character>(character);
            _context.Entry(domainCharacter).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CharacterExists(id))
                {
                    return NotFound($"No character in database with id {id}");

                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        /// <summary>
        /// Adds a new character to the database.
        /// </summary>
        /// <param name="character">A character object with the desired values to be added to the database</param>
        /// <returns>An action result with a statuscode of the request</returns>
        [HttpPost]
        public async Task<ActionResult<Character>> PostCharacter([FromBody] CharacterCreateDTO character)
        {
            var domainCharacter = _mapper.Map<Character>(character);
            try
            {

            _context.Characters.Add(domainCharacter);
            await _context.SaveChangesAsync();
            }
            catch
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
            var readCharacterDTO = _mapper.Map<CharacterReadDTO>(domainCharacter);
            return CreatedAtAction("GetCharacter", new { id = readCharacterDTO.Id }, readCharacterDTO);
        }

        /// <summary>
        /// Deletes a character.
        /// </summary>
        /// <param name="id">The character id to delete</param>
        /// <returns>An action result with a statuscode of the request</returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteCharacter(int id)
        {
            var character = await _context.Characters.FindAsync(id);
            if (character == null)
            {
                return NotFound($"No characters in database with id {id}");
            }

            _context.Characters.Remove(character);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool CharacterExists(int id)
        {
            return _context.Characters.Any(character => character.Id == id);
        }
    }
}
