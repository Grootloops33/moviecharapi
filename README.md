# MovieCharacterAPI
This projects features an API for reading, writing, updating and deleting of franchises, movies and characters from a database. The user can select a movie to view the characters for, select a franchise to see the movies for or select a franchise to see the characters for. 
# Install
In a terminal, clone the files to a custom folder and get your hostname (needed for SQL Server Management Studio) by running:
```
git clone https://gitlab.com/Grootloops33/moviecharapi.git
hostname
```
* Install [SQL Server Managment Studio (SSMS)](https://docs.microsoft.com/en-us/sql/ssms/download-sql-server-management-studio-ssms?redirectedfrom=MSDN&view=sql-server-ver16)
* Install [SQL Express](https://www.microsoft.com/en-us/Download/details.aspx?id=101064)
* Install [.NET 6.0](https://dotnet.microsoft.com/en-us/download/dotnet/5.0)
* Make sure all the following packages are installed in the solution (you can check under Dependencies > Packages):
  * Automapper (11.0.1)
  * AutoMapper.Extensions.Microsoft.DependencyInjection (11.0.0)
  * Microsoft.EntityFrameworkCore (6.0.8)
  * Microsoft.EntityFrameworkCore.Design (6.0.8)
  * Microsoft.EntityFrameworkCore.SqlServer (6.0.8)
  * Microsoft.EntityFrameworkCore.Tools (6.0.8)
  * Microsoft.VisualStudio.Web.CodeGeneration.Design (6.0.8)
  * Swashbuckle.AspNetCore (6.2.3)

# Usage
* Open SSMS and connect to YOUR-HOSTNAME/SQLEXPRESS
* Open the Package Manager Console (View > Other Windows > Package Manager Console) and run update-database
* Run the project, Swagger will open the interface automatically
* Send HTTP Requests using the Swagger interface to try out the API!

# Project Structure
* Controllers: The HTTP Controllers for the different models in the database
* Data: The Database Context needed for EntityFramework and the Seeded Data
* Migrations: Database migrations needed to run update-database to add the database, tables and seeded data
* Models: The Franchise, Movie and Character models for representing the data from the database and the DTO's needed for processing them
* Profiles: Configuration for AutoMapper to process from model to/from DTO

# Authors
Project created by Sten de Boer, David Nap and Fenna Ransijn
