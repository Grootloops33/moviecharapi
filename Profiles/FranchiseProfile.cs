﻿using AutoMapper;
using moviecharapi.Models.Domain;
using moviecharapi.Models.DTO.Franchise;

namespace moviecharapi.Profiles
{
    public class FranchiseProfile : Profile
    {
        public FranchiseProfile()
        {
            CreateMap<Franchise, FranchiseReadDTO>();
            CreateMap<FranchiseCreateDTO, Franchise>();
            CreateMap<FranchiseUpdateDTO, Franchise>();
            CreateMap<FranchiseDeleteDTO, Franchise>();
        }
    }
}
