﻿using AutoMapper;
using moviecharapi.Models.Domain;
using moviecharapi.Models.DTO.Movie;

namespace moviecharapi.Profiles
{
    public class MovieProfile : Profile
    {
        public MovieProfile()
        {
            CreateMap<Movie, MovieReadDTO>();
            CreateMap<MovieCreateDTO, Movie>();
            CreateMap<MovieUpdateDTO, MovieReadDTO>();
            CreateMap<MovieUpdateDTO, Movie>();
        }
    }
}

