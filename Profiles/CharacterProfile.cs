﻿using AutoMapper;
using moviecharapi.Models.Domain;
using moviecharapi.Models.DTO.Character;

namespace moviecharapi.Profiles
{
    public class CharacterProfile : Profile
    {
        public CharacterProfile()
        {
            CreateMap<Character, CharacterReadDTO>();
            CreateMap<CharacterCreateDTO, Character>();
            CreateMap<CharacterUpdateDTO, CharacterReadDTO>();
            CreateMap<CharacterUpdateDTO, Character>();
        }
    }
}

