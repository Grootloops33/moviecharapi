﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace moviecharapi.Migrations
{
    public partial class DBSetup : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Character",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FullName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    Alias = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    Gender = table.Column<string>(type: "nvarchar(1)", nullable: true),
                    Picture = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Character", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Franchises",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    Description = table.Column<string>(type: "nvarchar(500)", maxLength: 500, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Franchises", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Movies",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Title = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    Year = table.Column<int>(type: "int", nullable: false),
                    Director = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    Genre = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    Picture = table.Column<string>(type: "nvarchar(250)", maxLength: 250, nullable: true),
                    Trailer = table.Column<string>(type: "nvarchar(250)", maxLength: 250, nullable: true),
                    FranchiseId = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Movies", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Movies_Franchises_FranchiseId",
                        column: x => x.FranchiseId,
                        principalTable: "Franchises",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "CharacterMovie",
                columns: table => new
                {
                    MovieId = table.Column<int>(type: "int", nullable: false),
                    CharacterId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CharacterMovie", x => new { x.MovieId, x.CharacterId });
                    table.ForeignKey(
                        name: "FK_CharacterMovie_Character_CharacterId",
                        column: x => x.CharacterId,
                        principalTable: "Character",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CharacterMovie_Movies_MovieId",
                        column: x => x.MovieId,
                        principalTable: "Movies",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Character",
                columns: new[] { "Id", "Alias", "FullName", "Gender", "Picture" },
                values: new object[,]
                {
                    { 1, "Iron Man", "Tony Stark", "M", "https://static.wikia.nocookie.net/disney/images/9/96/Iron-Man-AOU-Render.png" },
                    { 2, "Thor", "Thor Odinson", "M", "https://www.tradeinn.com/f/13820/138203770/marvel-mezco-toys-the-avengers-thor-16-cm-figure.jpg" },
                    { 3, "Hulk", "Bruce Banner", "M", "https://cdn.vegaoo.nl/images/rep_art/gra/321/0/321071/plastic-hulk-avengers-figuurtje.jpg" },
                    { 4, "Hawkeye", "Clint Barton", "M", "https://www.hdwallpapersfreedownload.com/uploads/large/super-heroes/hawkeye-promo.jpg" },
                    { 5, "Captain America", "Steven Rogers", "M", "https://static.posters.cz/image/1300/posters/captain-america-under-fire-i127942.jpg" },
                    { 6, "Black Widow", "Natasha Romanoff", "F", "https://i.pinimg.com/originals/34/32/d8/3432d83661a718dd8858f60ac0396e6c.jpg" },
                    { 7, "Wormie", "Luke Skywalker", "M", "https://upload.wikimedia.org/wikipedia/en/9/9b/Luke_Skywalker.png" },
                    { 8, "Princess Leia", "Princess Leia Organa of Alderaan", "F", "https://upload.wikimedia.org/wikipedia/en/1/1b/Princess_Leia%27s_characteristic_hairstyle.jpg" },
                    { 9, null, "R2-D2", null, "https://a.1stdibscdn.com/r2d2-life-size-model-sculpture-star-wars-for-sale-picture-6/f_14282/1556547097614/Sans_titre_6_master.jpg?width=768" },
                    { 10, "Old Ben", "Obi-Wan Kenobi", "M", "https://upload.wikimedia.org/wikipedia/en/3/32/Ben_Kenobi.png" },
                    { 11, "Frodo", "Frodo Baggins", "M", "http://www.merrybrandybuck.com/immagini/Frodo/frodo80.jpg" },
                    { 12, "Gandalf", "Gandalf the Grey", "M", "https://3.bp.blogspot.com/-6XwNXHFaQJw/UrZOKiuoeBI/AAAAAAAACw0/poXkKi3BCnw/s1600/gandalf2.jpg" },
                    { 13, "Strider", "Aragorn Elessar II", "M", "https://pm1.narvii.com/6110/c883909a0e17a18ec2e7175cece57f001ba033f3_hq.jpg" },
                    { 14, "Sam", "Samwise Gamgee", "M", "https://vignette.wikia.nocookie.net/starwarsofthecaribbean/images/c/cb/LotR-TwoTowers_009.jpg/revision/latest/scale-to-width-down/2000?cb=20120228083120" }
                });

            migrationBuilder.InsertData(
                table: "Franchises",
                columns: new[] { "Id", "Description", "Name" },
                values: new object[,]
                {
                    { 1, "The Marvel Cinematic Universe is an American media franchise and shared universe centered on a series of superhero films produced by Marvel Studios. The films are based on characters that appear in American comic books published by Marvel Comics.", "Marvel Cinematic Universe" },
                    { 2, "Star Wars is an American epic space-opera multimedia franchise created by George Lucas, which began with the eponymous 1977 film and quickly became a worldwide pop-culture phenomenon.", "Star Wars" },
                    { 3, "The Lord of the Rings is a series of three epic fantasy adventure films directed by Peter Jackson, based on the novel written by J.R.R. Tolkien. The films are subtitled The Fellowship of the Ring, The Two Towers, and The Return of the King.", "The Lord of the Rings" }
                });

            migrationBuilder.InsertData(
                table: "Movies",
                columns: new[] { "Id", "Director", "FranchiseId", "Genre", "Picture", "Title", "Trailer", "Year" },
                values: new object[,]
                {
                    { 1, "Jon Favreau", 1, "Action", "https://m.media-amazon.com/images/M/MV5BMTczNTI2ODUwOF5BMl5BanBnXkFtZTcwMTU0NTIzMw@@._V1_FMjpg_UX1000_.jpg", "Iron Man 1", "https://www.youtube.com/watch?v=8ugaeA-nMTc", 2008 },
                    { 2, "Louis Leterrier", 1, "Action", "http://dangerousuniverse.com/wp-content/uploads/2017/05/The-Incredible-Hulk-1300x1912.jpg", "The Incredible Hulk", "https://www.youtube.com/watch?v=xbqNb2PFKKA", 2008 },
                    { 3, "Jon Favreau", 1, "Action", "https://media.s-bol.com/7NqmOZlVODA/843x1200.jpg", "Iron Man 2", "https://www.youtube.com/watch?v=BoohRoVA9WQ", 2010 },
                    { 4, "Kenneth Branagh", 1, "Action", "https://image.tmdb.org/t/p/original/prSfAi1xGrhLQNxVSUFh61xQ4Qy.jpg", "Thor", "https://www.youtube.com/watch?v=JOddp-nlNvQ", 2011 },
                    { 5, "Joe Johnston", 1, "Action", "https://image.tmdb.org/t/p/original/kHbLKbMIL1w9WhofQlM03FQrQHI.jpg", "Captain America: The First Avenger", "https://www.youtube.com/watch?v=JerVrbLldXw", 2011 },
                    { 6, "Joss Whedon", 1, "Action", "https://prod-ripcut-delivery.disney-plus.net/v1/variant/disney/32BF5941765B045734A8DE2CDEF30EF5F940F26119A94BA4677414A4406ACFC9/scale?width=1200&aspectRatio=1.78&format=jpeg", "Marvel's The Avengers", "https://www.youtube.com/watch?v=eOrNdBpGMv8", 2012 },
                    { 7, "Peter Jackson", 3, "Fantasy", "https://www.blackgate.com/wp-content/uploads/2014/01/The-Fellowship-of-the-Ring-poster.jpg", "The Lord of the Rings: Fellowship of the Ring", "https://www.youtube.com/watch?v=V75dMMIW2B4", 2001 },
                    { 8, "Peter Jackson", 3, "Fantasy", "https://3.bp.blogspot.com/-nqW9cI6_htQ/UD87erPxIuI/AAAAAAAAMKY/pM52X2YzocQ/s1600/The+Two+Towers+(2002)+6.jpg", "The Lord of the Rings: The Two Towers", "https://www.youtube.com/watch?v=hYcw5ksV8YQ", 2002 },
                    { 9, "Peter Jackson", 3, "Fantasy", "https://2.bp.blogspot.com/-djIuucN9UUM/TbKeWpumHrI/AAAAAAAAA70/ZlNcrYG3G6I/s1600/The-Lord-of-the-Rings-The-Return-of-the-King-movie-poster.jpg", "The Lord of the Rings: The Return of the King", "https://www.youtube.com/watch?v=r5X-hFf6Bwo", 2003 },
                    { 10, "George Lucas", 2, "Science-Fiction", "https://www.vintagemovieposters.co.uk/wp-content/uploads/2017/11/IMG_8957.jpg", "A New Hope", "https://www.youtube.com/watch?v=vZ734NWnAHA", 1977 },
                    { 11, "George Lucas", 2, "Science-Fiction", "https://i.pinimg.com/originals/ed/1e/1a/ed1e1a918e2f14dab18e74781d98aed4.jpg", "The Empire Strikes Back", "https://www.youtube.com/watch?v=JNwNXF9Y6kY", 1980 },
                    { 12, "George Lucas", 2, "Science-Fiction", "https://image.tmdb.org/t/p/original/ydjp1K13GrnbiX0yjd398BI9xaC.jpg", "Return of the Jedi", "https://www.youtube.com/watch?v=7L8p7_SLzvU", 1983 }
                });

            migrationBuilder.InsertData(
                table: "CharacterMovie",
                columns: new[] { "CharacterId", "MovieId" },
                values: new object[,]
                {
                    { 1, 1 },
                    { 3, 2 },
                    { 1, 3 },
                    { 2, 4 },
                    { 5, 5 },
                    { 1, 6 },
                    { 2, 6 },
                    { 3, 6 },
                    { 4, 6 },
                    { 5, 6 },
                    { 6, 6 },
                    { 11, 7 },
                    { 12, 7 },
                    { 13, 7 },
                    { 14, 7 },
                    { 11, 8 },
                    { 12, 8 },
                    { 13, 8 },
                    { 14, 8 },
                    { 11, 9 },
                    { 12, 9 },
                    { 13, 9 },
                    { 14, 9 },
                    { 7, 10 },
                    { 8, 10 },
                    { 9, 10 },
                    { 10, 10 },
                    { 7, 11 },
                    { 8, 11 },
                    { 9, 11 },
                    { 10, 11 },
                    { 7, 12 },
                    { 8, 12 },
                    { 9, 12 },
                    { 10, 12 }
                });

            migrationBuilder.CreateIndex(
                name: "IX_CharacterMovie_CharacterId",
                table: "CharacterMovie",
                column: "CharacterId");

            migrationBuilder.CreateIndex(
                name: "IX_Movies_FranchiseId",
                table: "Movies",
                column: "FranchiseId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CharacterMovie");

            migrationBuilder.DropTable(
                name: "Character");

            migrationBuilder.DropTable(
                name: "Movies");

            migrationBuilder.DropTable(
                name: "Franchises");
        }
    }
}
