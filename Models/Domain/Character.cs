﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace moviecharapi.Models.Domain
{
    [Table("Character")]
    public class Character
    {
        public int Id { get; set; }
        [MaxLength(50)]
        [Required]
        public string FullName { get; set; }
        [MaxLength(50)]
        public string? Alias { get; set; }
        public char? Gender { get; set; }
        [MaxLength(200)]
        public string Picture { get; set; }
        public ICollection<CharacterMovie> Movies { get; set; }
    }
}
