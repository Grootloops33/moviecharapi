﻿using System.ComponentModel.DataAnnotations;

namespace moviecharapi.Models.Domain
{
    public class Franchise
    {
        public int Id { get; set; }
        [MaxLength(50)]
        [Required]
        public string Name { get; set; }
        [MaxLength(500)]
        public string? Description { get; set; }

        public ICollection<Movie> Movies { get; set; }
    }
}
