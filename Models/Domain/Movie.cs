﻿using System.ComponentModel.DataAnnotations;

namespace moviecharapi.Models.Domain
{
    public class Movie
    {
        public int Id { get; set; }
        [MaxLength(50)]
        public string Title { get; set; }
        public int Year { get; set; }
        [MaxLength(50)]
        public string? Director { get; set; }
        [MaxLength(100)]
        public string? Genre { get; set; }
        [MaxLength(250)]
        public string? Picture { get; set; }
        [MaxLength(250)]
        public string? Trailer { get; set; }
        public ICollection<CharacterMovie>? Characters { get; set; }
        public int? FranchiseId { get; set; }
        public Franchise Franchise { get; set; }
    }
}
