﻿using System.ComponentModel.DataAnnotations;

namespace moviecharapi.Models.DTO.Franchise
{
    public class FranchiseCreateDTO
    {
        [MaxLength(50, ErrorMessage = "Name contains too many characters. Max lenght is: 50")]
        public string Name { get; set; }

        [MaxLength(200, ErrorMessage = "Description contains too many characters. Max lenght is: 200")]
        public string Description { get; set; }
    }
}
