﻿using System.ComponentModel.DataAnnotations;

namespace moviecharapi.Models.DTO.Franchise
{
    public class FranchiseReadDTO
    {
        public int Id { get; set; }

        [MaxLength(100, ErrorMessage = "Name contains too many characters. Max lenght is: 100")]
        public string Name { get; set; }

        [MaxLength(100, ErrorMessage = "Description contains too many characters. Max lenght is: 100")]
        public string Description { get; set; }
    }
}
