﻿namespace moviecharapi.Models.DTO.Franchise
{
    public class FranchiseDeleteDTO
    {
        public int Id { get; set; }
    }
}
