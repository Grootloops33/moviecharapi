﻿namespace moviecharapi.Models.DTO.Character
{
    public class CharacterReadDTO
    {
        public int Id { get; set; }
        public string FullName { get; set; }
        public string Alias { get; set; }
        public char? Gender { get; set; }
        public string Picture { get; set; }
    }
}
