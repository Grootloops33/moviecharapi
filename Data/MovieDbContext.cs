﻿using Microsoft.EntityFrameworkCore;
using moviecharapi.Models.Domain;

namespace moviecharapi.Data
{
    public class MovieDbContext : DbContext
    {
        public DbSet<Movie> Movies { get; set; }
        public DbSet<Franchise> Franchises { get; set; }
        public DbSet<Character> Characters { get; set; }
        public DbSet<CharacterMovie> CharacterMovie { get; set; }
        public MovieDbContext(DbContextOptions options) : base(options)
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<CharacterMovie>().HasKey(cm => new { cm.MovieId, cm.CharacterId });
            modelBuilder.Entity<CharacterMovie>()
                .HasOne(ch => ch.Character)
                .WithMany(m => m.Movies)
                .HasForeignKey(ch => ch.CharacterId);
            modelBuilder.Entity<CharacterMovie>()
                .HasOne(mov => mov.Movie)
                .WithMany(m => m.Characters)
                .HasForeignKey(m => m.MovieId);

            modelBuilder.Entity<Character>().HasData(SeedHelper.GetSeedCharacters());
            modelBuilder.Entity<Franchise>().HasData(SeedHelper.GetSeedFranchises());
            modelBuilder.Entity<Movie>().HasData(SeedHelper.GetSeedMovies());
            modelBuilder.Entity<CharacterMovie>().HasData(SeedHelper.GetSeedCharacterMovie());
        }
    }
}
