﻿using moviecharapi.Models.Domain;

namespace moviecharapi.Data
{
    public class SeedHelper
    {
        public static List<CharacterMovie> GetSeedCharacterMovie()
        {
            List<CharacterMovie> characterMovieSeeds = new List<CharacterMovie>()
            {
                new CharacterMovie
                {
                    MovieId = 1,
                    CharacterId = 1
                },
                new CharacterMovie
                {
                    MovieId = 2,
                    CharacterId = 3
                },
                new CharacterMovie
                {
                    MovieId = 3,
                    CharacterId = 1
                },
                new CharacterMovie
                {
                    MovieId = 4,
                    CharacterId = 2
                },
                new CharacterMovie
                {
                    MovieId = 5,
                    CharacterId = 5
                },
                new CharacterMovie
                {
                    MovieId = 6,
                    CharacterId = 1
                },
                new CharacterMovie
                {
                    MovieId = 6,
                    CharacterId = 2,
                },
                new CharacterMovie
                {
                    MovieId = 6,
                    CharacterId = 3
                },
                new CharacterMovie
                {
                    MovieId = 6,
                    CharacterId = 4,
                },
                new CharacterMovie
                {
                    MovieId = 6,
                    CharacterId = 5,
                },
                new CharacterMovie
                {
                    MovieId = 6,
                    CharacterId = 6
                },
                new CharacterMovie
                {
                    MovieId = 7,
                    CharacterId = 11
                },
                new CharacterMovie
                {
                    MovieId = 7,
                    CharacterId = 12
                },
                new CharacterMovie
                {
                    MovieId = 7,
                    CharacterId = 13
                },
                new CharacterMovie
                {
                    MovieId = 7,
                    CharacterId = 14
                },
                new CharacterMovie
                {
                    MovieId = 8,
                    CharacterId = 11
                },
                new CharacterMovie
                {
                    MovieId = 8,
                    CharacterId = 12
                },
                new CharacterMovie
                {
                    MovieId = 8,
                    CharacterId = 13
                },
                new CharacterMovie
                {
                    MovieId = 8,
                    CharacterId = 14
                },
                new CharacterMovie
                {
                    MovieId = 9,
                    CharacterId = 11
                },
                new CharacterMovie
                {
                    MovieId = 9,
                    CharacterId = 12
                },
                new CharacterMovie
                {
                    MovieId = 9,
                    CharacterId = 13
                },
                new CharacterMovie
                {
                    MovieId = 9,
                    CharacterId = 14
                },
                new CharacterMovie
                {
                    MovieId = 10,
                    CharacterId = 7
                },
                new CharacterMovie
                {
                    MovieId = 10,
                    CharacterId = 8
                },
                new CharacterMovie
                {
                    MovieId = 10,
                    CharacterId = 9
                },
                new CharacterMovie
                {
                    MovieId = 10,
                    CharacterId = 10
                },
                new CharacterMovie
                {
                    MovieId = 11,
                    CharacterId = 7
                },
                new CharacterMovie
                {
                    MovieId = 11,
                    CharacterId = 8
                },
                new CharacterMovie
                {
                    MovieId = 11,
                    CharacterId = 9
                },
                new CharacterMovie
                {
                    MovieId = 11,
                    CharacterId = 10
                },
                new CharacterMovie
                {
                    MovieId = 12,
                    CharacterId = 7
                },
                new CharacterMovie
                {
                    MovieId = 12,
                    CharacterId = 8
                },
                new CharacterMovie
                {
                    MovieId = 12,
                    CharacterId = 9
                },
                new CharacterMovie
                {
                    MovieId = 12,
                    CharacterId = 10
                },
            };
            return characterMovieSeeds;
        }
        public static List<Character> GetSeedCharacters()
        {
            List<Character> characterSeeds = new List<Character>()
                {
                    new Character
                    {
                        Id = 1,
                        FullName = "Tony Stark",
                        Alias = "Iron Man",
                        Gender = 'M',
                        Picture = "https://static.wikia.nocookie.net/disney/images/9/96/Iron-Man-AOU-Render.png"
                    },
                    new Character
                    {
                        Id = 2,
                        FullName = "Thor Odinson",
                        Alias = "Thor",
                        Gender = 'M',
                        Picture = "https://www.tradeinn.com/f/13820/138203770/marvel-mezco-toys-the-avengers-thor-16-cm-figure.jpg"
                    },
                    new Character {
                        Id = 3,
                        FullName = "Bruce Banner",
                        Alias = "Hulk",
                        Gender = 'M',
                        Picture = "https://cdn.vegaoo.nl/images/rep_art/gra/321/0/321071/plastic-hulk-avengers-figuurtje.jpg"
                    },
                    new Character
                    {
                        Id = 4,
                        FullName = "Clint Barton",
                        Alias = "Hawkeye",
                        Gender = 'M',
                        Picture = "https://www.hdwallpapersfreedownload.com/uploads/large/super-heroes/hawkeye-promo.jpg"
                    },
                    new Character
                    {
                        Id = 5,
                        FullName = "Steven Rogers",
                        Alias = "Captain America",
                        Gender = 'M',
                        Picture = "https://static.posters.cz/image/1300/posters/captain-america-under-fire-i127942.jpg"
                    },
                    new Character
                    {
                        Id = 6,
                        FullName = "Natasha Romanoff",
                        Alias = "Black Widow",
                        Gender = 'F',
                        Picture = "https://i.pinimg.com/originals/34/32/d8/3432d83661a718dd8858f60ac0396e6c.jpg"
                    },
                    new Character
                    {
                        Id = 7,
                        FullName = "Luke Skywalker",
                        Alias = "Wormie",
                        Gender = 'M',
                        Picture = "https://upload.wikimedia.org/wikipedia/en/9/9b/Luke_Skywalker.png"
                    },
                    new Character
                    {
                        Id = 8,
                        FullName = "Princess Leia Organa of Alderaan",
                        Alias = "Princess Leia",
                        Gender = 'F',
                        Picture = "https://upload.wikimedia.org/wikipedia/en/1/1b/Princess_Leia%27s_characteristic_hairstyle.jpg"
                    },
                    new Character
                    {
                        Id = 9,
                        FullName = "R2-D2",
                        Picture = "https://a.1stdibscdn.com/r2d2-life-size-model-sculpture-star-wars-for-sale-picture-6/f_14282/1556547097614/Sans_titre_6_master.jpg?width=768"
                    },
                    new Character
                    {
                        Id = 10,
                        FullName = "Obi-Wan Kenobi",
                        Alias = "Old Ben",
                        Gender = 'M',
                        Picture = "https://upload.wikimedia.org/wikipedia/en/3/32/Ben_Kenobi.png"
                    },
                    new Character
                    {
                        Id = 11,
                        FullName = "Frodo Baggins",
                        Alias = "Frodo",
                        Gender = 'M',
                        Picture = "http://www.merrybrandybuck.com/immagini/Frodo/frodo80.jpg"
                    },
                    new Character
                    {
                        Id = 12,
                        FullName = "Gandalf the Grey",
                        Alias = "Gandalf",
                        Gender = 'M',
                        Picture = "https://3.bp.blogspot.com/-6XwNXHFaQJw/UrZOKiuoeBI/AAAAAAAACw0/poXkKi3BCnw/s1600/gandalf2.jpg"
                    },
                    new Character
                    {
                        Id = 13,
                        FullName = "Aragorn Elessar II",
                        Alias = "Strider",
                        Gender = 'M',
                        Picture = "https://pm1.narvii.com/6110/c883909a0e17a18ec2e7175cece57f001ba033f3_hq.jpg"
                    },
                    new Character
                    {
                        Id = 14,
                        FullName = "Samwise Gamgee",
                        Alias = "Sam",
                        Gender = 'M',
                        Picture = "https://vignette.wikia.nocookie.net/starwarsofthecaribbean/images/c/cb/LotR-TwoTowers_009.jpg/revision/latest/scale-to-width-down/2000?cb=20120228083120"
                    }
                };
            return characterSeeds;
        }
        public static List<Franchise> GetSeedFranchises()
        {
            List<Franchise> franchiseSeeds = new List<Franchise>()
            {
                new Franchise
                {
                    Id = 1,
                    Name = "Marvel Cinematic Universe",
                    Description = "The Marvel Cinematic Universe is an American media franchise and shared universe centered on a series of superhero films produced by Marvel Studios. The films are based on characters that appear in American comic books published by Marvel Comics."
                },
                new Franchise
                {
                    Id = 2,
                    Name = "Star Wars",
                    Description = "Star Wars is an American epic space-opera multimedia franchise created by George Lucas, which began with the eponymous 1977 film and quickly became a worldwide pop-culture phenomenon."
                },
                new Franchise
                {
                    Id = 3,
                    Name = "The Lord of the Rings",
                    Description = "The Lord of the Rings is a series of three epic fantasy adventure films directed by Peter Jackson, based on the novel written by J.R.R. Tolkien. The films are subtitled The Fellowship of the Ring, The Two Towers, and The Return of the King."
                }
            };
            return franchiseSeeds;
        }
        public static List<Movie> GetSeedMovies()
        {
            List<Movie> movieSeeds = new List<Movie>()
            {
                new Movie
                {
                    Id = 1,
                    Title = "Iron Man 1",
                    Genre = "Action",
                    Year = 2008,
                    Director = "Jon Favreau",
                    Picture = "https://m.media-amazon.com/images/M/MV5BMTczNTI2ODUwOF5BMl5BanBnXkFtZTcwMTU0NTIzMw@@._V1_FMjpg_UX1000_.jpg",
                    Trailer = "https://www.youtube.com/watch?v=8ugaeA-nMTc",
                    FranchiseId = 1
                },
                new Movie
                {
                    Id = 2,
                    Title = "The Incredible Hulk",
                    Genre = "Action",
                    Year = 2008,
                    Director = "Louis Leterrier",
                    Picture = "http://dangerousuniverse.com/wp-content/uploads/2017/05/The-Incredible-Hulk-1300x1912.jpg",
                    Trailer = "https://www.youtube.com/watch?v=xbqNb2PFKKA",
                    FranchiseId = 1
                },
                new Movie
                {
                    Id = 3,
                    Title = "Iron Man 2",
                    Genre = "Action",
                    Year = 2010,
                    Director = "Jon Favreau",
                    Picture = "https://media.s-bol.com/7NqmOZlVODA/843x1200.jpg",
                    Trailer = "https://www.youtube.com/watch?v=BoohRoVA9WQ",
                    FranchiseId = 1
                },
                new Movie
                {
                    Id = 4,
                    Title = "Thor",
                    Genre= "Action",
                    Year = 2011,
                    Director = "Kenneth Branagh",
                    Picture = "https://image.tmdb.org/t/p/original/prSfAi1xGrhLQNxVSUFh61xQ4Qy.jpg",
                    Trailer = "https://www.youtube.com/watch?v=JOddp-nlNvQ",
                    FranchiseId = 1
                },
                new Movie
                {
                    Id = 5,
                    Title = "Captain America: The First Avenger",
                    Genre="Action",
                    Year = 2011,
                    Director = "Joe Johnston",
                    Picture = "https://image.tmdb.org/t/p/original/kHbLKbMIL1w9WhofQlM03FQrQHI.jpg",
                    Trailer = "https://www.youtube.com/watch?v=JerVrbLldXw",
                    FranchiseId = 1
                },
                new Movie
                {
                    Id = 6,
                    Title = "Marvel's The Avengers",
                    Genre = "Action",
                    Year = 2012,
                    Director = "Joss Whedon",
                    Picture = "https://prod-ripcut-delivery.disney-plus.net/v1/variant/disney/32BF5941765B045734A8DE2CDEF30EF5F940F26119A94BA4677414A4406ACFC9/scale?width=1200&aspectRatio=1.78&format=jpeg",
                    Trailer = "https://www.youtube.com/watch?v=eOrNdBpGMv8",
                    FranchiseId = 1
                },
                new Movie
                {
                    Id = 7,
                    Title = "The Lord of the Rings: Fellowship of the Ring",
                    Genre = "Fantasy",
                    Year = 2001,
                    Director = "Peter Jackson",
                    Picture = "https://www.blackgate.com/wp-content/uploads/2014/01/The-Fellowship-of-the-Ring-poster.jpg",
                    Trailer = "https://www.youtube.com/watch?v=V75dMMIW2B4",
                    FranchiseId = 3
                },
                new Movie
                {
                    Id = 8,
                    Title = "The Lord of the Rings: The Two Towers",
                    Genre = "Fantasy",
                    Year = 2002,
                    Director = "Peter Jackson",
                    Picture = "https://3.bp.blogspot.com/-nqW9cI6_htQ/UD87erPxIuI/AAAAAAAAMKY/pM52X2YzocQ/s1600/The+Two+Towers+(2002)+6.jpg",
                    Trailer = "https://www.youtube.com/watch?v=hYcw5ksV8YQ",
                    FranchiseId = 3
                },
                new Movie
                {
                    Id = 9,
                    Title = "The Lord of the Rings: The Return of the King",
                    Genre = "Fantasy",
                    Year = 2003,
                    Director = "Peter Jackson",
                    Picture = "https://2.bp.blogspot.com/-djIuucN9UUM/TbKeWpumHrI/AAAAAAAAA70/ZlNcrYG3G6I/s1600/The-Lord-of-the-Rings-The-Return-of-the-King-movie-poster.jpg",
                    Trailer = "https://www.youtube.com/watch?v=r5X-hFf6Bwo",
                    FranchiseId = 3
                },
                new Movie
                {
                    Id = 10,
                    Title = "A New Hope",
                    Genre = "Science-Fiction",
                    Year = 1977,
                    Director = "George Lucas",
                    Picture = "https://www.vintagemovieposters.co.uk/wp-content/uploads/2017/11/IMG_8957.jpg",
                    Trailer = "https://www.youtube.com/watch?v=vZ734NWnAHA",
                    FranchiseId = 2
                },
                new Movie
                {
                    Id = 11,
                    Title = "The Empire Strikes Back",
                    Genre = "Science-Fiction",
                    Year = 1980,
                    Director = "George Lucas",
                    Picture = "https://i.pinimg.com/originals/ed/1e/1a/ed1e1a918e2f14dab18e74781d98aed4.jpg",
                    Trailer = "https://www.youtube.com/watch?v=JNwNXF9Y6kY",
                    FranchiseId = 2
                },
                new Movie
                {
                    Id = 12,
                    Title = "Return of the Jedi",
                    Genre = "Science-Fiction",
                    Year = 1983,
                    Director = "George Lucas",
                    Picture = "https://image.tmdb.org/t/p/original/ydjp1K13GrnbiX0yjd398BI9xaC.jpg",
                    Trailer = "https://www.youtube.com/watch?v=7L8p7_SLzvU",
                    FranchiseId = 2
                }
            };
            return movieSeeds;
        }
    }
}
